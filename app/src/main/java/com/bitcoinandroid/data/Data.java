package com.bitcoinandroid.data;

public class Data
{
    private String pending_value;

    private String address;

    private String balance;

    private String total_txs;

    private Txs[] txs;

    private String network;

    private String received_value;

    public String getPending_value ()
    {
        return pending_value;
    }

    public void setPending_value (String pending_value)
    {
        this.pending_value = pending_value;
    }

    public String getAddress ()
    {
        return address;
    }

    public void setAddress (String address)
    {
        this.address = address;
    }

    public String getBalance ()
    {
        return balance;
    }

    public void setBalance (String balance)
    {
        this.balance = balance;
    }

    public String getTotal_txs ()
    {
        return total_txs;
    }

    public void setTotal_txs (String total_txs)
    {
        this.total_txs = total_txs;
    }

    public Txs[] getTxs ()
    {
        return txs;
    }

    public void setTxs (Txs[] txs)
    {
        this.txs = txs;
    }

    public String getNetwork ()
    {
        return network;
    }

    public void setNetwork (String network)
    {
        this.network = network;
    }

    public String getReceived_value ()
    {
        return received_value;
    }

    public void setReceived_value (String received_value)
    {
        this.received_value = received_value;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [pending_value = "+pending_value+", address = "+address+", balance = "+balance+", total_txs = "+total_txs+", txs = "+txs+", network = "+network+", received_value = "+received_value+"]";
    }
}
