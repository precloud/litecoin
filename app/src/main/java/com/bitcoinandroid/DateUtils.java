package com.bitcoinandroid;

import java.text.SimpleDateFormat;
import java.util.Locale;

public class DateUtils {
    public static String unixToDate(String unix_timestamp) {
        long timestamp = Long.parseLong(unix_timestamp) * 1000;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy MMM d hh:mm:ss", Locale.ENGLISH);
        return sdf.format(timestamp);
    }
}
