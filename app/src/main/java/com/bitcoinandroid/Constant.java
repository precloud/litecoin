package com.bitcoinandroid;

public class Constant {
    public static final String URL_MAIN = "https://testnet.litecore.io/api/";
    //public static final String URL_MAIN = "https://chain.so/api/v2/";
    public static final String GET_BALANCE = "addr/%s";
    public static final String ADDRESS = "addr/%s";
    public static final String TRANSACTIONS = "addrs/%s/txs?from=%s&to=%s";
    public static final String COIN_TYPE = "LTCTEST"; //LTCTEST //LTC
    public static final String SUCCESS = "success";
}
