/**
 * Copyright 2011 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.bitcoin.examples;


import com.google.bitcoin.bouncycastle.util.encoders.Hex;
import com.google.bitcoin.core.Address;
import com.google.bitcoin.core.AddressFormatException;
import com.google.bitcoin.core.NetworkParameters;
import com.google.bitcoin.core.Utils;

import java.util.Arrays;

public class AddressTest {
    static final NetworkParameters testParams = NetworkParameters.testNet();
    static final NetworkParameters prodParams = NetworkParameters.prodNet();

    public static void main(String[] args) {
        stringification();
    }
    public static void stringification() {
        // Test a testnet address.
        Address a = new Address(testParams, Hex.decode("fda79a24e50ff70ff42f7d89585da5bd19d9e5cc"));
        System.out.println(a.toString());
        Address b = new Address(prodParams, Hex.decode("3f2ebb6c8d88e586b551303d2c29eba15518d8d1"));
    }

    public void decoding() throws Exception {
        Address a = new Address(testParams, "n4eA2nbYqErp7H6jebchxAN59DmNpksexv");

        Address b = new Address(prodParams, "LQz2pJYaeqntA9BFB8rDX5AL2TTKGd5AuN");
    }
    

    public void getNetwork() throws Exception {
        /*NetworkParameters params = Address.getParametersFromAddress("LQz2pJYaeqntA9BFB8rDX5AL2TTKGd5AuN");
        assertEquals(NetworkParameters.prodNet().getId(), params.getId());
        params = Address.getParametersFromAddress("n4eA2nbYqErp7H6jebchxAN59DmNpksexv");
        assertEquals(NetworkParameters.testNet().getId(), params.getId());*/
    }
}
