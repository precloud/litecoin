package com.bitcoinandroid.data;

public class Incoming {
    private String script_asm;

    private String script_hex;

    private String output_no;

    private Inputs[] inputs;

    private String spent;

    private String req_sigs;

    private String value;

    public String getScript_asm ()
    {
        return script_asm;
    }

    public void setScript_asm (String script_asm)
    {
        this.script_asm = script_asm;
    }

    public String getScript_hex ()
    {
        return script_hex;
    }

    public void setScript_hex (String script_hex)
    {
        this.script_hex = script_hex;
    }

    public String getOutput_no ()
    {
        return output_no;
    }

    public void setOutput_no (String output_no)
    {
        this.output_no = output_no;
    }

    public Inputs[] getInputs ()
    {
        return inputs;
    }

    public void setInputs (Inputs[] inputs)
    {
        this.inputs = inputs;
    }

    public String getSpent ()
    {
        return spent;
    }

    public void setSpent (String spent)
    {
        this.spent = spent;
    }

    public String getReq_sigs ()
    {
        return req_sigs;
    }

    public void setReq_sigs (String req_sigs)
    {
        this.req_sigs = req_sigs;
    }

    public String getValue ()
    {
        return value;
    }

    public void setValue (String value)
    {
        this.value = value;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [script_asm = "+script_asm+", script_hex = "+script_hex+", output_no = "+output_no+", inputs = "+inputs+", spent = "+spent+", req_sigs = "+req_sigs+", value = "+value+"]";
    }
}
