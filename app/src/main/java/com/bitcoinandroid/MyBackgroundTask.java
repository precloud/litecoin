package com.bitcoinandroid;

import android.os.AsyncTask;

import okhttp3.Callback;

public class MyBackgroundTask extends AsyncTask<Void,Void,Void> {
    private String url;
    private Callback callback;
    public MyBackgroundTask(String url, Callback callback) {
        this.url = url;
        this.callback = callback;
    }

    protected Void doInBackground(Void... voids) {
        new CallService().call(url,callback);
        return null;
    }
}
