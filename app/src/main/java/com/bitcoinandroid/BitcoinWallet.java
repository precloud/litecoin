package com.bitcoinandroid;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigInteger;
import java.util.ArrayList;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.bitcoinandroid.buy.Buy1Activity;
import com.bitcoinandroid.data.AddressResponse;
import com.google.bitcoin.core.Address;
import com.google.bitcoin.core.ECKey;
import com.google.bitcoin.core.ScriptException;
import com.google.bitcoin.core.Transaction;
import com.google.bitcoin.core.TransactionInput;
import com.google.bitcoin.core.Utils;
import com.google.bitcoin.core.Wallet.BalanceType;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.zxing.client.android.CaptureActivity;

import org.json.JSONObject;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

import static com.bitcoinandroid.Constant.TRANSACTIONS;
import static com.bitcoinandroid.Constant.URL_MAIN;

public class BitcoinWallet extends Activity /*implements CallHistory*/{
	
	BackgroundTask backgroundTask;
	ProgressBar spinner;
	ApplicationState appState;
	SharedPreferences settings;
	Address address;
	TextView tvPriceLite,tvPriceUsd;
	RecyclerView rvHistory;
	HistoryAdapter adapter;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		activity = this;
		appState = ((ApplicationState) getApplication());
		settings = getPreferences(MODE_PRIVATE);
		ECKey key = appState.wallet.keychain.get(0);
		address = key.toAddress(appState.params);
		setContentView(R.layout.light_coin_detail_page);
		Button sendButton =  this.findViewById(R.id.send_button);
		Button mbBuy =  this.findViewById(R.id.mbBuy);
		Button mbSell =  this.findViewById(R.id.mbSell);
		tvPriceLite =  this.findViewById(R.id.tvPriceLite);
		tvPriceUsd =  this.findViewById(R.id.tvPriceUsd);
		rvHistory =  this.findViewById(R.id.rvHistory);
		adapter = new HistoryAdapter(this);
		rvHistory.setAdapter(adapter);
		rvHistory.setLayoutManager(new LinearLayoutManager(this));
		mbBuy.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				startActivity(new Intent(BitcoinWallet.this, Buy1Activity.class));
			}
		});
		sendButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				startActivity(new Intent(BitcoinWallet.this, CaptureActivity.class));
			}
		});

		CardView receiveButton =  this.findViewById(R.id.receive_button);
		receiveButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				startActivity(new Intent(BitcoinWallet.this, ReceiveMoney.class));
			}
		});

		new SetBalance(activity,tvPriceLite,tvPriceUsd,appState);

		try {
			//Block tx = new Block(appState.params,bitcoinSerialize());
			//moneyReceived(tx.cloneAsHeader());
		} catch (Exception e) {
			e.printStackTrace();
		}
		//Transaction tx = new Transaction(appState.params);
		
		//updateUI();
		//updateBlockChain();

		/*appState.wallet.addEventListener(new WalletEventListener() {
			public void onCoinsReceived(Wallet w, final Transaction tx, BigInteger prevBalance, BigInteger newBalance) {
				runOnUiThread(new Runnable() {
					public void run() {
						moneyReceived(tx);
						Log.d("Wallet", "COINS WE RECEIVED HAVE BEEN CONFIRMED");
					}
				});
			}
			
			public void onCoinsSent(Wallet w, final Transaction tx, BigInteger prevBalance, BigInteger newBalance) {
				runOnUiThread(new Runnable() {
					public void run() {
						updateUI();
						appState.saveWallet();
						Log.d("Wallet", "COINS WE SENT HAVE BEEN CONFIRMED");
					}
				});
			}

			public void onPendingCoinsReceived(Wallet w, final Transaction tx) {
				runOnUiThread(new Runnable() {
					public void run() {
						moneyReceived(tx);
						Log.d("Wallet", "PENDING COINS RECEIVED ");
					}
				});
			}
		});*/
	}
	public byte[] bitcoinSerialize() {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		try {
			bitcoinSerializeToStream(stream);
		} catch (IOException e) {
			// Cannot happen, we are serializing to a memory stream.
			throw new RuntimeException(e);
		}
		return stream.toByteArray();
	}

	/**
	 * Serializes this message to the provided stream. If you just want the raw bytes use bitcoinSerialize().
	 */
	void bitcoinSerializeToStream(OutputStream stream) throws IOException {
	}
	Activity activity;


	private void updateBlockChain() {
		if (backgroundTask == null || backgroundTask.getStatus() != AsyncTask.Status.RUNNING) {
			backgroundTask = new BackgroundTask(this);
			backgroundTask.execute();
			((ProgressBar)findViewById(R.id.progressBar)).setVisibility(View.VISIBLE);
		} else if (backgroundTask.getStatus() == AsyncTask.Status.RUNNING){
			backgroundTask.downloadingProgress.show();
		}
	}
	
	public void hideSpinner() {
		((ProgressBar)findViewById(R.id.progressBar)).setVisibility(View.GONE);
	}

	private void updateUI() {
		if (appState == null || appState.wallet == null) {
			return;
		}
		TextView balance = (TextView) findViewById(R.id.balanceLabel);
		balance.setText("BTC " + Utils.bitcoinValueToFriendlyString(appState.wallet.getBalance(BalanceType.ESTIMATED)));

		TableLayout tl = (TableLayout) findViewById(R.id.transactions);
		tl.removeAllViews();
		ArrayList<Transaction> transactions = appState.wallet.getAllTransactions();
		
		// Show only the first 100 transaction
		if (transactions.size() > 100)
			transactions = (ArrayList<Transaction>)transactions.subList(0, 99);
		
		for (Transaction tx : transactions) {
			addRowForTransaction(tl, tx);
		}
	}

	private void addRowForTransaction(TableLayout tl, Transaction tx) {
		// Create a new row to be added.
		TableRow tr = new TableRow(this);
		// Create a Button to be the row-content.
		TextView description = new TextView(this);
		TextView amount = new TextView(this);
		description.setTextSize(15);
		amount.setTextSize(15);
		String text = "";

		// check if pending
		if (appState.wallet.pending.containsKey(tx.getHash())) {
			text += "(Pending) ";
			description.setTextColor(Color.GRAY);
			amount.setTextColor(Color.GRAY);
		} else {
			description.setTextColor(Color.BLACK);
			amount.setTextColor(Color.BLACK);
		}

		// check if sent or received
		try {
			if (tx.sent(appState.wallet)) {
				text += "Sent to " + tx.outputs.get(0).getScriptPubKey().getToAddress();
				amount.setText("-" + Utils.bitcoinValueToFriendlyString(tx.amount(appState.wallet)));
			} else {
				text += "Received from " + tx.getInputs().get(0).getFromAddress();
				amount.setText("+" + Utils.bitcoinValueToFriendlyString(tx.amount(appState.wallet)));
			}
		} catch (ScriptException e) {
			// don't display this transaction
			return;
		}
		if (text.length() > 30) {
			text = text.substring(0, 29) + "...";
		}
		description.setText(text);
		description.setPadding(0, 3, 0, 3);
		amount.setPadding(0, 3, 0, 3);
		amount.setGravity(Gravity.RIGHT);
		tr.addView(description);
		tr.addView(amount);
		tl.addView(tr, new TableLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
	}

	public void callTransaction(int from, int to) {
        String url = URL_MAIN + String.format(TRANSACTIONS,address.toString(),String.valueOf(from),String.valueOf(to));
		//Log.d("TransResCall",url);
        new MyBackgroundTask(url,new Callback(){

            public void onFailure(Call call, IOException e) {
				try {
					Toast.makeText(activity,"Error In Network",Toast.LENGTH_LONG).show();
				}catch (Exception ee){
					ee.getLocalizedMessage();
				}

            }

            public void onResponse(Call call, Response response) throws IOException {
                try {
					JSONObject jo = new JSONObject(response.body().string());
					Gson gson = new Gson();
					/*JsonElement element = gson.fromJson(jo.toString(), JsonElement.class);
					Log.d("TransRes",response.body().string());
					String data = new String(response.body().string());
                    Log.d("TransResData",new StringBuilder(response.body().string()).toString());
                    Log.d("TransResData",jo.toString());
                    Log.d("TransResData","isFound");*/
					FeedTransaction feedTransaction = gson.fromJson(jo.toString(), FeedTransaction.class);
					/*if (feedTransaction!=null)
						Log.d("TransResData1","Sucess");
					else
						Log.d("TransResData1","Err");
					if (feedTransaction.getItems()!=null)
						Log.d("TransResData2","Sucess");
					else
						Log.d("TransResData2","Err");*/
                    adapter.list.addAll(feedTransaction.getItems());
                    if ((adapter.list.size())==feedTransaction.getTotalItems()||adapter.list.isEmpty()){
                    	adapter.isListEnd = true;
					}
					adapter.isProcessing = false;
					//Log.d("TransResData3","Sucess"+adapter.getItemCount());
					runOnUiThread(new Runnable() {
						public void run() {
							adapter.notifyDataSetChanged();
						}
					});

					//Log.d("TransResData13","Sucess"+adapter.getItemCount());
                    return;
                } catch (Exception e) {
					//Log.d("TransResData23",e.getLocalizedMessage());
                    e.printStackTrace();
                }
                try {
					adapter.isListEnd = true;
					runOnUiThread(new Runnable() {
						public void run() {
							adapter.notifyDataSetChanged();
						}
					});
                	/*runOnUiThread(new Runnable() {
						public void run() {
							Toast.makeText(activity,"Error In Network",Toast.LENGTH_LONG).show();
						}
					});*/
                }catch (Exception e){
                    e.getLocalizedMessage();
                }
            }
        }).execute();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {
		case R.id.refresh_menu_item:
			updateBlockChain();
			updateUI();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void moneyReceived(Transaction tx) {
		appState.saveWallet();
		updateUI();
		
		if (appState.notifiedUserOfTheseTransactions.contains(tx.getHash())) {
			//only call once per transaction
			//this could be called multiple times by separate peers
			return;
		} else {
			appState.notifiedUserOfTheseTransactions.add(tx.getHash());
		}
		
		TransactionInput input = tx.getInputs().get(0);
		Address from;
		try {
			from = input.getFromAddress();
		} catch (ScriptException e) {
			return;
		}
		BigInteger value = tx.getValueSentToMe(appState.wallet);
		Log.d("Wallet", "Received " + Utils.bitcoinValueToFriendlyString(value) + " from " + from.toString());

		String ticker = Utils.bitcoinValueToFriendlyString(value) + " Bitcoins Received!";
		String ns = Context.NOTIFICATION_SERVICE;
		NotificationManager notificationManager = (NotificationManager) getSystemService(ns);
		Notification notification = new Notification(R.drawable.my_notification_icon, ticker, System.currentTimeMillis());
		notification.defaults |= Notification.DEFAULT_SOUND;
		notification.defaults |= Notification.DEFAULT_VIBRATE;
		notification.flags |= Notification.FLAG_AUTO_CANCEL;

		Context context = getApplicationContext();
		CharSequence contentTitle = ticker;
		CharSequence contentText = "From " + from.toString();
		Intent notificationIntent = new Intent(this, BitcoinWallet.class);
		//don't open a new one if it's already on top
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
		//notification.setLatestEventInfo(context, contentTitle, contentText, contentIntent);
		notificationManager.notify(1, notification);
	}

	/*public void result(AddressResponse response) {
		try {
			ArrayList<FeedHistoryTransaction> list = new ArrayList<FeedHistoryTransaction>();
			for (String txs:response.getTransactions()
				 ) {
				try {
					FeedHistoryTransaction f = new FeedHistoryTransaction();
					f.txId = txs;
					*//*f.date = DateUtils.unixToDate(txs.getTime());
					f.isIncoming = true;
					f.price = String.format(activity.getResources().getString(R.string.val_ltc2),Float.parseFloat(txs.getIncoming().getValue()),"");*//*
					list.add(f);
				}catch (Exception ee){
					ee.getLocalizedMessage();
				}
			}
			adapter.list = new ArrayList<FeedHistoryTransaction>();
			adapter.list.addAll(list);
			adapter.notifyDataSetChanged();
		}catch (Exception e){
			e.getLocalizedMessage();
		}
	}*/
}