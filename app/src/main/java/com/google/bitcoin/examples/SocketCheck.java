package com.google.bitcoin.examples;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;

public class SocketCheck {
    public static void main(String[] args) {
        try {
            String strAdd = "192.168.1.9";
            int port = 3001;
            int connectTimeout = 6667;
            System.out.println("Start");
            System.out.println("Connecting...");
            InetSocketAddress address = new InetSocketAddress(strAdd, port);
            Socket socket = new Socket(InetAddress.getByName("192.168.1.9"), 3001);
            if(!socket.isConnected())
                socket.connect(address, connectTimeout);

            /*OutputStream out = socket.getOutputStream();
            out.write(InetAddress.getLocalHost().getHostName().getBytes());
            out.write(8080);*/
            InputStream in = socket.getInputStream();
            System.out.println("Reading Data...");
            final int bufferSize = 1024;
            final char[] buffer = new char[bufferSize];
            final StringBuilder outS = new StringBuilder();
            Reader inr = new InputStreamReader(in, "UTF-8");
            for (; ; ) {
                int rsz = inr.read(buffer, 0, buffer.length);
                if (rsz < 0)
                    break;
                outS.append(buffer, 0, rsz);
            }
            inr.close();
            System.out.println("Reading Data Success");
            System.out.println(outS.toString());

           // Socket socket = new Socket("wss://echo.websocket.org/",8080);

            System.out.println(socket.isConnected());
            System.out.println("End");
            System.out.println("Closing All...");
            in.close();
            //out.close();
            socket.close();
            System.out.println("Closing All Complete");
        } catch (Exception e) {
            System.out.println("Error:"+e.getLocalizedMessage());
            e.printStackTrace();
        }finally {

        }
    }
}
