package com.bitcoinandroid;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.MyViewHolder>{
    ArrayList<FeedTransactionResult> list =new ArrayList<FeedTransactionResult>();
    private BitcoinWallet bitcoinWallet;
    private boolean isEmpty = true;
    public boolean isListEnd = false;
    private int from = 0,to;
    private int limit = 10;
    public boolean isProcessing = false;
    public HistoryAdapter(BitcoinWallet bitcoinWallet) {
        this.bitcoinWallet=bitcoinWallet;
    }

    @NonNull
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        if (i==0){
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.toolbar_main,viewGroup,false);
            return new MyViewHolderHead(view);
        }else if (i==1){
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.progress_bar,viewGroup,false);
            return new MyViewHolderBar(view);
        }else {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.toolbar_main_item,viewGroup,false);
            return new MyViewHolder(view);
        }

    }

    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        if (myViewHolder instanceof MyViewHolderBar){
            if (!isProcessing){
                isProcessing = true;
                from = list.size();
                to = list.size()+limit;
                bitcoinWallet.callTransaction(from,to);
            }
        }else if (myViewHolder instanceof MyViewHolderHead){

        }else {
            if (i > 0) {
                i--;
                FeedTransactionResult g = list.get(i);
                myViewHolder.tvDate.setText(DateUtils.unixToDate(g.getTime()));
                String t = "";
                if (g.getLocktime()==0){
                    t = "+";
                    myViewHolder.ivIcon.setImageResource(R.drawable.ic_arrow_drop_up_green_24dp);
                }else {
                    t = "-";
                    myViewHolder.ivIcon.setImageResource(R.drawable.ic_arrow_drop_down_red_24dp);
                }
                String sb = t +
                        g.getValueIn();
                myViewHolder.tvPrice.setText(sb);
                //myViewHolder.tvId.setText(String.valueOf(g.getBlockheight()));
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position==0)return 0;
        if (!isListEnd)
            if ((position-1)==list.size())return 1;
        return 3;
    }

    public int getItemCount() {
        int i = 1;
        if (!isListEnd)
            i++;
        return list.size()+i;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView ivIcon;
        TextView tvId;
        TextView tvDate;
        TextView tvPrice;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ivIcon = itemView.findViewById(R.id.ivIcon);
            tvId = itemView.findViewById(R.id.tvId);
            tvDate = itemView.findViewById(R.id.tvDate);
            tvPrice = itemView.findViewById(R.id.tvPrice);
        }
    }

    private class MyViewHolderHead extends MyViewHolder {
        public MyViewHolderHead(View view) {
            super(view);
        }
    }

    private class MyViewHolderBar extends MyViewHolder {
        public MyViewHolderBar(View view) {
            super(view);
        }
    }
}
