package com.bitcoinandroid;

import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.bitcoinandroid.data.AddressResponse;
import com.google.bitcoin.core.Address;
import com.google.bitcoin.core.ECKey;
import com.google.gson.Gson;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

import static com.bitcoinandroid.Constant.ADDRESS;
import static com.bitcoinandroid.Constant.COIN_TYPE;
import static com.bitcoinandroid.Constant.SUCCESS;
import static com.bitcoinandroid.Constant.URL_MAIN;

public class SetBalance {
    private Activity activity;
    private TextView tvPriceLite,tvPriceUsd;
    private Address address;
    private ApplicationState appState;
    String balance;
    public SetBalance(Activity activity, TextView tvPriceLite,TextView tvPriceUsd, ApplicationState appState) {
        this.activity = activity;
        this.tvPriceLite = tvPriceLite;
        this.tvPriceUsd = tvPriceUsd;
        ECKey key = appState.wallet.keychain.get(0);
        address = key.toAddress(appState.params);
        this.appState = appState;
        callBalance();
    }
    private void callBalance() {
        String url = URL_MAIN + String.format(ADDRESS,address.toString());
        new MyBackgroundTask(url,new Callback(){

            public void onFailure(Call call, IOException e) {
                activity.runOnUiThread(new Runnable() {
                    public void run() {
                        try {
                            Toast.makeText(activity,"Error In Network",Toast.LENGTH_LONG).show();
                        }catch (Exception e){
                            e.getLocalizedMessage();
                        }
                    }
                });

            }

            public void onResponse(Call call, Response response) throws IOException {
                try {
                    Gson gson = new Gson();
                    appState.addressResponse = gson.fromJson(response.body().string(), AddressResponse.class);
                    AddressResponse result = appState.addressResponse;
                    try {
                        activity.runOnUiThread(new Runnable() {
                            public void run() {
                                try {
                                    ((CallHistory) activity).result(appState.addressResponse);
                                }catch (Exception e){
                                    e.printStackTrace();
                                }
                            }
                        });
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    balance = String.valueOf(result.getBalance());
                    callUsd();
                    activity.runOnUiThread(new Runnable() {
                        public void run() {
                            tvPriceLite.setText(String.format(activity.getResources().getString(R.string.val_ltc2),Float.parseFloat(balance), COIN_TYPE));
                        }
                    });
                    return;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                activity.runOnUiThread(new Runnable() {
                    public void run() {
                        try {
                            Toast.makeText(activity,"Error In Network",Toast.LENGTH_LONG).show();
                        }catch (Exception e){
                            e.getLocalizedMessage();
                        }
                    }
                });
            }
        }).execute();
    }

    private void callUsd() {
        //https://www.bitstamp.net/api/v2/ticker/ltcusd/
        String url = "https://www.bitstamp.net/api/v2/ticker/ltcusd/";
        new MyBackgroundTask(url,new Callback(){
            public void onFailure(Call call, IOException e) {}

            public void onResponse(Call call, Response response) throws IOException {
                try {
                    Gson gson = new Gson();
                    final UsdResponse usdResponse = gson.fromJson(response.body().string(), UsdResponse.class);
                    //Log.d("hiii", String.valueOf(usdResponse.last));
                    activity.runOnUiThread(new Runnable() {
                        public void run() {
                            try {
                                float val = Float.valueOf(balance) * usdResponse.last;
                                tvPriceUsd.setText(String.valueOf(val) + " USD");
                                tvPriceUsd.setVisibility(View.VISIBLE);
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    });
                    return;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).execute();
    }
}
