package com.bitcoinandroid;

import java.util.ArrayList;

public class FeedTransaction {
    private float totalItems;
    private float from;
    private float to;

    public ArrayList<FeedTransactionResult> getItems() {
        return items;
    }

    public void setItems(ArrayList<FeedTransactionResult> items) {
        this.items = items;
    }

    private ArrayList <FeedTransactionResult> items = new ArrayList<FeedTransactionResult>();


    // Getter Methods

    public float getTotalItems() {
        return totalItems;
    }

    public float getFrom() {
        return from;
    }

    public float getTo() {
        return to;
    }

    // Setter Methods

    public void setTotalItems(float totalItems) {
        this.totalItems = totalItems;
    }

    public void setFrom(float from) {
        this.from = from;
    }

    public void setTo(float to) {
        this.to = to;
    }
}
