package com.bitcoinandroid.data;

public class Received_from {
    private String output_no;

    private String txid;

    public String getOutput_no ()
    {
        return output_no;
    }

    public void setOutput_no (String output_no)
    {
        this.output_no = output_no;
    }

    public String getTxid ()
    {
        return txid;
    }

    public void setTxid (String txid)
    {
        this.txid = txid;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [output_no = "+output_no+", txid = "+txid+"]";
    }
}
