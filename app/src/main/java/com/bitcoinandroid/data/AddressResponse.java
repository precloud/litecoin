package com.bitcoinandroid.data;

import java.util.ArrayList;

public class AddressResponse {
    private String addrStr;
    private float balance;
    private float balanceSat;
    private float totalReceived;
    private float totalReceivedSat;
    private float totalSent;
    private float totalSentSat;
    private float unconfirmedBalance;
    private float unconfirmedBalanceSat;
    private float unconfirmedTxApperances;
    private float txApperances;

    public ArrayList<String> getTransactions() {
        return transactions;
    }

    public void setTransactions(ArrayList<String> transactions) {
        this.transactions = transactions;
    }

    private ArrayList<String> transactions = new ArrayList <String> ();


    // Getter Methods

    public String getAddrStr() {
        return addrStr;
    }

    public float getBalance() {
        return balance;
    }

    public float getBalanceSat() {
        return balanceSat;
    }

    public float getTotalReceived() {
        return totalReceived;
    }

    public float getTotalReceivedSat() {
        return totalReceivedSat;
    }

    public float getTotalSent() {
        return totalSent;
    }

    public float getTotalSentSat() {
        return totalSentSat;
    }

    public float getUnconfirmedBalance() {
        return unconfirmedBalance;
    }

    public float getUnconfirmedBalanceSat() {
        return unconfirmedBalanceSat;
    }

    public float getUnconfirmedTxApperances() {
        return unconfirmedTxApperances;
    }

    public float getTxApperances() {
        return txApperances;
    }

    // Setter Methods

    public void setAddrStr(String addrStr) {
        this.addrStr = addrStr;
    }

    public void setBalance(float balance) {
        this.balance = balance;
    }

    public void setBalanceSat(float balanceSat) {
        this.balanceSat = balanceSat;
    }

    public void setTotalReceived(float totalReceived) {
        this.totalReceived = totalReceived;
    }

    public void setTotalReceivedSat(float totalReceivedSat) {
        this.totalReceivedSat = totalReceivedSat;
    }

    public void setTotalSent(float totalSent) {
        this.totalSent = totalSent;
    }

    public void setTotalSentSat(float totalSentSat) {
        this.totalSentSat = totalSentSat;
    }

    public void setUnconfirmedBalance(float unconfirmedBalance) {
        this.unconfirmedBalance = unconfirmedBalance;
    }

    public void setUnconfirmedBalanceSat(float unconfirmedBalanceSat) {
        this.unconfirmedBalanceSat = unconfirmedBalanceSat;
    }

    public void setUnconfirmedTxApperances(float unconfirmedTxApperances) {
        this.unconfirmedTxApperances = unconfirmedTxApperances;
    }

    public void setTxApperances(float txApperances) {
        this.txApperances = txApperances;
    }
}
