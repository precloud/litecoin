package com.bitcoinandroid.data;

public class Inputs {
    private Received_from received_from;

    private String address;

    private String input_no;

    public Received_from getReceived_from ()
    {
        return received_from;
    }

    public void setReceived_from (Received_from received_from)
    {
        this.received_from = received_from;
    }

    public String getAddress ()
    {
        return address;
    }

    public void setAddress (String address)
    {
        this.address = address;
    }

    public String getInput_no ()
    {
        return input_no;
    }

    public void setInput_no (String input_no)
    {
        this.input_no = input_no;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [received_from = "+received_from+", address = "+address+", input_no = "+input_no+"]";
    }
}
