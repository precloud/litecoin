package com.bitcoinandroid;

import java.math.BigInteger;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.button.MaterialButton;
import android.support.design.widget.TextInputEditText;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.bitcoin.core.Address;
import com.google.bitcoin.core.AddressFormatException;
import com.google.bitcoin.core.ECKey;
import com.google.bitcoin.core.Transaction;
import com.google.bitcoin.core.Utils;

public class SendMoney extends Activity {

	EditText addressField;
	EditText amountField;
	EditText memoField;

	Address address;
	BigInteger amount;
	TextView tvPriceLite,tvPriceUsd;
	ApplicationState appState;

	/** Called when the activity is first created */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.light_coin_send);
		appState = ((ApplicationState) getApplication());
		ECKey key = appState.wallet.keychain.get(0);
		address = key.toAddress(appState.params);
		Bundle b = getIntent().getExtras();
		addressField =  findViewById(R.id.address);
		amountField =  findViewById(R.id.amount);
		memoField =  findViewById(R.id.memo);
		tvPriceLite =  this.findViewById(R.id.tvPriceLite);
		tvPriceUsd =  this.findViewById(R.id.tvPriceUsd);
		ImageView ivBack =  findViewById(R.id.ivBack);
		ivBack.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				onBackPressed();
			}
		});
		if (b == null) {
			addressField.requestFocus();
		} else {
			if (b.getString("address") != null) {
				addressField.setText(b.getString("address"));
			}
			if (b.getString("amount") != null) {
				String amount = b.getString("amount");
				if (amount.toLowerCase().endsWith("x8")) {
					amount = amount.toLowerCase().replace("x8", "");
				}
				amountField.setText(amount);
			}
			if (b.getString("label") != null) {
				memoField.setText(URLDecoder.decode(b.getString("label")) + " ");
			}
			if (b.getString("message") != null) {
				memoField.setText(memoField.getText() + URLDecoder.decode(b.getString("message")));
			}
			if (b.getString("memo") != null) {
				memoField.setText(memoField.getText() + URLDecoder.decode(b.getString("memo")));
			}
			memoField.setText(memoField.getText().toString().trim());
		}
		
		if (addressField.getText().toString() == "") {
			addressField.requestFocus();
		} else {
			amountField.requestFocus();
		}
		new SetBalance(this,tvPriceLite,tvPriceUsd,appState);
		Button sendButton =  this.findViewById(R.id.send_money_button);
		sendButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Transaction sendTx = null;
				ApplicationState appState = ApplicationState.current;

				try {
					address = new Address(appState.params, addressField.getText().toString());
					amount = Utils.toNanoCoins(amountField.getText().toString());
					sendTx = appState.wallet.createSend(address, amount);
                    /*BlockCypherContext context = new BlockCypherContext("v1", "ltc", "main", "9062a8a84c91463684034f09bf8d0642");
// WIF Format of your private Key
                    String myPrivateKey = appState.wallet.keychain.get(0).toAddress2(appState.params);
                    IntermediaryTransaction unsignedTx = context.getTransactionService()
                            .newTransaction(
                                    new ArrayList<String>(Arrays.asList("2MvYVGGsnDg5c6dqb3tV3zv8K8wnHXnT9wH")),
                                    new ArrayList<String>(Arrays.asList("2MvYVGGsnDg5c6dqb3tV3zv8K8wnHXnT9wH")),
                                    10
                            );
                    SignUtils.signWithBase58KeyWithPubKey(unsignedTx, myPrivateKey);

                    com.blockcypher.src.main.java.com.blockcypher.model.transaction.Transaction tx = context.getTransactionService().sendTransaction(unsignedTx);

                    System.out.println("Sent transaction: " + GsonFactory.getGsonPrettyPrint().toJson(tx));*/
					if (sendTx != null) {
						Log.d("Transss", String.valueOf(sendTx.getHash().toString()));

						appState.sendTransaction(sendTx);
						Log.d("Wallet", "Sent " + Utils.bitcoinValueToFriendlyString(amount) + " to " + address.toString());
						
						AlertDialog.Builder builder = new AlertDialog.Builder(SendMoney.this);
						builder.setMessage(R.string.send_money_payment_successful).setCancelable(false).setNegativeButton(R.string.send_money_ok, new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
								startActivity(new Intent(SendMoney.this, BitcoinWallet.class));
							}
						});
						AlertDialog alert = builder.create();
						alert.show();
					} else {
						AlertDialog.Builder builder = new AlertDialog.Builder(SendMoney.this);
						StringBuilder msg = new StringBuilder(getString(R.string.send_money_insufficient_funds));
						if (appState.wallet.getPendingTransactions().size() > 0) {
							msg.append(getString(R.string.send_money_xfer_pending));
						} else {
							msg.append(getString(R.string.send_money_try_funding));
						}
						builder.setMessage(msg)
								.setCancelable(false).setNegativeButton(R.string.send_money_ok, new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog, int id) {
										dialog.cancel();
										startActivity(new Intent(SendMoney.this, BitcoinWallet.class));
									}
								});
						AlertDialog alert = builder.create();
						alert.show();
					}
				} catch (AddressFormatException e) {
					e.printStackTrace();
					AlertDialog.Builder builder = new AlertDialog.Builder(SendMoney.this);
					builder.setMessage(R.string.send_money_invalid_address).setCancelable(false).setNegativeButton(R.string.send_money_ok, new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							dialog.cancel();
						}
					});
					AlertDialog alert = builder.create();
					alert.show();
				} catch (Exception e) {
					e.printStackTrace();
					AlertDialog.Builder builder = new AlertDialog.Builder(SendMoney.this);
					builder.setMessage(R.string.send_money_fail).setCancelable(false)
							.setNegativeButton(R.string.send_money_ok, new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int id) {
									dialog.cancel();
								}
							});
					AlertDialog alert = builder.create();
					alert.show();
				}

			}
		});

	}

}
