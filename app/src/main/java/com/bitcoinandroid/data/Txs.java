package com.bitcoinandroid.data;

public class Txs {
    private String block_no;

    private Incoming incoming;

    private String txid;

    private String time;

    private String confirmations;

    public String getBlock_no ()
    {
        return block_no;
    }

    public void setBlock_no (String block_no)
    {
        this.block_no = block_no;
    }

    public Incoming getIncoming ()
    {
        return incoming;
    }

    public void setIncoming (Incoming incoming)
    {
        this.incoming = incoming;
    }

    public String getTxid ()
    {
        return txid;
    }

    public void setTxid (String txid)
    {
        this.txid = txid;
    }

    public String getTime ()
    {
        return time;
    }

    public void setTime (String time)
    {
        this.time = time;
    }

    public String getConfirmations ()
    {
        return confirmations;
    }

    public void setConfirmations (String confirmations)
    {
        this.confirmations = confirmations;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [block_no = "+block_no+", incoming = "+incoming+", txid = "+txid+", time = "+time+", confirmations = "+confirmations+"]";
    }
}
