package com.bitcoinandroid;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class CallService {

    /*public static void main(String[] args) {
        call("https://chain.so/api/v2/get_address_balance/LTCTEST/2MvYVGGsnDg5c6dqb3tV3zv8K8wnHXnT9wH/1",
                new Callback() {
                    public void onFailure(Call call, IOException e) {
                        System.out.println(e.getLocalizedMessage());
                    }

                    public void onResponse(Call call, AddressResponse response) throws IOException {
                        System.out.println(response.body().string());
                    }
                });
    }*/
    public void call(String url,Callback okhttpCallback){
        OkHttpClient client = new OkHttpClient().newBuilder()
                .connectTimeout(120,TimeUnit.SECONDS)
                .readTimeout(60,TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.MINUTES)
                .build();
        Request okHttprequest = new Request.Builder()
                .url(url)
                .build();
        client.newCall(okHttprequest).enqueue(okhttpCallback);
    }
}
