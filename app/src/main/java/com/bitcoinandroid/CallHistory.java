package com.bitcoinandroid;

import com.bitcoinandroid.data.AddressResponse;

interface CallHistory {
    void result(AddressResponse response);
}
